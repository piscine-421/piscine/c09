#!/bin/sh

mkdir objects
cd objects
cc -Wall -Werror -Wextra -c ../*.c
cd ..
ar rcs libft.a objects/*.o
rm -rf objects
